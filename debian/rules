#!/usr/bin/make -f
# -*- makefile -*-

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

include /usr/share/dpkg/pkg-info.mk

DEB_VERSION_UPSTREAM := $(patsubst %+repack,%,$(DEB_VERSION_UPSTREAM))

# Increase hardening to maximum, considering attack surface
export DEB_BUILD_MAINT_OPTIONS = hardening=+all

%:
	dh $@

override_dh_auto_configure:
	dh_auto_configure \
		-O--buildsystem=cmake -- \
		-DUSE_BUNDLED_DEPS=OFF \
		-DUSE_BUNDLED_VALIJSON=OFF \
		-DBUILD_SHARED_LIBS=ON \
		-DBUILD_DRIVER=OFF     \
		-DVALIJSON_INCLUDE=/usr/include/valijson \
		-DFALCOSECURITY_LIBS_VERSION="$(DEB_VERSION_UPSTREAM)" \
		-DBUILD_LIBSCAP_GVISOR=OFF \
		-DBUILD_LIBSCAP_EXAMPLES=OFF \
		-DBUILD_LIBSINSP_EXAMPLES=OFF \
		-DCREATE_TEST_TARGETS=ON \
		-DENABLE_DRIVERS_TESTS=ON \
		-DENABLE_IA32_TESTS=OFF \
		-DENABLE_LIBSCAP_TESTS=ON \
		-DWITH_CHISEL=ON \
		-DDRIVER_VERSION=$(DEB_VERSION_UPSTREAM) \
		-DSCAP_FILES_SUITE_ENABLE=OFF \
		-DENABLE_LIBSINSP_E2E_TESTS=OFF \
		-DCHISEL_TOOL_LIBRARY_NAME="sysdig"


override_dh_auto_install-indep:
	dh_auto_install -i
	dh_dkms -i -pfalcosecurity-scap-dkms -- debian/tmp/usr/src/scap-$(DEB_VERSION_UPSTREAM)/dkms.conf

ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
override_dh_auto_test:
	$(MAKE) -C obj-$(DEB_HOST_GNU_TYPE) run-unit-tests
endif

# Everything is small. Compressing creates extra work for little benefit, so I
# don't do it
override_dh_compress:
	dh_compress -X.c -X.h -XCMakeLists.txt -XREADME

